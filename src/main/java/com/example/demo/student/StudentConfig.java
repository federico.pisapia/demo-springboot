package com.example.demo.student;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

import static java.util.Calendar.APRIL;
@Configuration
public class StudentConfig {
    @Bean
    CommandLineRunner commandLineRunner(StudentRepository repository) {
        return args -> {
            Student federico = new Student(1L, "federico", LocalDate.of(2000, APRIL, 5), 21, "federico@oncode.it");
            Student luca = new Student( "luca", LocalDate.of(2000, APRIL, 5), 21, "federico@oncode.it");
            repository.saveAll(List.of(federico,luca));
        };
    }
}
