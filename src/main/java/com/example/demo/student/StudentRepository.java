package com.example.demo.student;

import org.hibernate.sql.Select;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Optional;

@Repository
public interface StudentRepository extends JpaRepository <Student, Long> {
//"select s from student s where s.email=?1"
    @Query("select s from Student s where s.email=?1 ")
    Optional<Student> findStudentByEmail(String email);

    /*@Modifying
    @Query("update Student s set s.age=?1, s.LocalDate=?2, s.email=?3, s.name=?4, where s.id=?5")
    void SetStudentInfoById(int age, LocalDate dob, String email, String name, Long id);*/
}
